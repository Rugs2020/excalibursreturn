﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerPrefs", menuName = "Player/Player Preferences")]
public class PlayerPrefs : ScriptableObject
{
    public string m_Resolution;
    public bool m_FullScreen;
    [Range(-80.0f, 0.0f)]
    public float m_MusicVolume;
    [Range(-80.0f, 0.0f)]
    public float m_SfxVolume;
    [Range(-80.0f, 0.0f)]
    public float m_UIVolume;
}
