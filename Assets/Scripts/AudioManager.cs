﻿using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.Audio;

public enum SoundType { UI = 0, Music = 1, Arthur = 2, Excalibur = 4, Environment = 5 }

public class AudioManager : MonoBehaviour
{
    public static AudioManager s_Instance;
    public AudioMixer m_AudioMixer;

    public List<GameSound> m_Sounds;
    private Dictionary<SoundType, Dictionary<string, GameSound>> m_GameSounds;

    private void Awake()
    {
        if (s_Instance != null)
        {
            Destroy(this);
            Debug.LogWarning("An instance of AudioManager already exists in the scene. New instance has been destroyed.");
            return;
        }

        s_Instance = this;

        m_GameSounds = new Dictionary<SoundType, Dictionary<string, GameSound>>();
        InitSounds();

        PlaySound("Main Menu Music");
    }

    private void InitSounds()
    {
        foreach (GameSound sound in m_Sounds)
            AddSound(sound.m_Type, sound);
    }

    public void AddSound(SoundType category, params GameSound[] sounds)
    {
        if (!m_GameSounds.ContainsKey(category))
            m_GameSounds.Add(category, new Dictionary<string, GameSound>());

        var innerDictionary = m_GameSounds[category];
        foreach (GameSound sound in sounds)
        {
            if (innerDictionary.ContainsKey(sound.m_Name))
                Debug.LogWarning($"Sound ID [{sound.m_Name}] matches sound ID already in [{category}] sound list and has not been added.");
            else
            {
                sound.m_Parent = this.gameObject;
                sound.InitSound();
                innerDictionary.Add(sound.m_Name, sound);
                switch (sound.m_Type)
                {
                    case SoundType.UI:
                        sound.m_AudioSource.outputAudioMixerGroup = m_AudioMixer.FindMatchingGroups("UI")[0];
                        break;

                    case SoundType.Music:
                        sound.m_AudioSource.outputAudioMixerGroup = m_AudioMixer.FindMatchingGroups("Music")[0];
                        break;

                    case SoundType.Arthur:
                    case SoundType.Excalibur:
                    case SoundType.Environment:
                        sound.m_AudioSource.outputAudioMixerGroup = m_AudioMixer.FindMatchingGroups("Sfx")[0];
                        break;

                    default:
                        break;
                }

                Debug.Log($"Sound [{sound.m_Name}] added to [{category}] sound list.");
            }
        }
    }

    public void UpdateMusicVolume(float volume) => m_AudioMixer.SetFloat("MusicVolume", volume);
    public void UpdateSfxVolume(float volume) => m_AudioMixer.SetFloat("SfxVolume", volume);
    public void UpdateUIVolume(float volume) => m_AudioMixer.SetFloat("UIVolume", volume);

    public void PlaySound(string soundID)
    {
        foreach (var category in m_GameSounds.Values)
        {
            if (category.ContainsKey(soundID))
            {
                category[soundID].m_AudioSource.Play();
                return;
            }
        }

        Debug.LogWarning("No sound found.");
    }
}

[System.Serializable]
public class GameSound
{
    public AudioClip m_AudioClip;
    public string m_Name = "Sound";
    public SoundType m_Type;
    [Range(0.0f, 1.0f)]
    public float m_Volume = 1.0f;
    [Range(0.1f, 3.0f)]
    public float m_Pitch = 1.0f;
    public bool m_Loop = false;
    public bool m_Mute = false;

    [HideInInspector]
    public AudioSource m_AudioSource;
    [HideInInspector]
    public GameObject m_Parent;

    public void InitSound()
    {
        m_AudioSource = m_Parent.AddComponent<AudioSource>();
        m_AudioSource.clip = m_AudioClip;
        m_AudioSource.volume = m_Volume;
        m_AudioSource.pitch = m_Pitch;
        m_AudioSource.loop = m_Loop;
        m_AudioSource.mute = m_Mute;
    }
}