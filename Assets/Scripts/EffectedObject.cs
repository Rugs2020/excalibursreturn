﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EffectedObject : MonoBehaviour
{
    public enum ObjectType { door, movingPlatform, loopingPlatform, runtimeLoopingPlatform }
    public ObjectType m_Type;

    public float m_MoveSpeed = 0.25f;
    public Transform m_PositionA, m_PositionB;

    // for the looping platforms
    private int m_TargetPosition = 0;
    private Transform[] m_MovePositions;

    public Vector3 m_Movement;

    public Action Activate;
    public Action Deactivate;

    private void Awake()
    {
        m_MovePositions = new Transform[2] { m_PositionA, m_PositionB };

        switch (m_Type)
        {
            case ObjectType.door:
                Activate += DoorOpen;
                Deactivate += DoorClose;
                break;

            case ObjectType.movingPlatform:
                Activate += PlatformMove;
                Deactivate += PlatformReturn;
                break;

            case ObjectType.loopingPlatform:
                Activate += MoveLoopingPlatform;
                Deactivate += EmptyFunction;
                break;

            case ObjectType.runtimeLoopingPlatform:
                Activate += MoveLoopingPlatform;
                Deactivate += EmptyFunction;
                break;

        }
    }

    private void FixedUpdate()
    {
        transform.position += m_Movement;

        if (m_Type == ObjectType.runtimeLoopingPlatform)
        {
            MoveLoopingPlatform();
        }
    }

    // the basic door movement
    private void DoorOpen()
    {
        m_Movement = Vector3.Lerp(transform.position, m_PositionB.position, m_MoveSpeed) - transform.position;
        transform.rotation = Quaternion.Lerp(transform.rotation, m_PositionB.rotation, m_MoveSpeed);
    }
    private void DoorClose()
    {
        m_Movement = Vector3.Lerp(transform.position, m_PositionA.position, m_MoveSpeed) - transform.position;
        transform.rotation = Quaternion.Lerp(transform.rotation, m_PositionA.rotation, m_MoveSpeed);
    }

    // the basic falling platform movement
    private void PlatformMove() => m_Movement = Vector3.MoveTowards(transform.position, m_PositionB.position, m_MoveSpeed * Time.fixedDeltaTime) - transform.position;
    private void PlatformReturn() => m_Movement = Vector3.MoveTowards(transform.position, m_PositionA.position, m_MoveSpeed * Time.fixedDeltaTime) - transform.position;

    // the basic moving platform movement
    private void MoveLoopingPlatform()
    {
        // move the platform towards its next position
        m_Movement = Vector3.MoveTowards(transform.position, m_MovePositions[m_TargetPosition].position, m_MoveSpeed * Time.fixedDeltaTime) - transform.position;

        // change the platforms target
        if (transform.position == m_MovePositions[m_TargetPosition].position)
            m_TargetPosition += m_TargetPosition >= m_MovePositions.Length - 1 ? -m_TargetPosition : 1; // wrap the number so it will loop
    }

    // this is a pain but it's needed to stop null referances when deactivating
    private void EmptyFunction() { }
}
