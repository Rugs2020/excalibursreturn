﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public Dropdown m_ResolutionDropDown;
    public Toggle m_FullscreenToggle;
    public Slider m_MusicSilder;
    public Slider m_SfxSilder;
    public Slider m_UISilder;

    private void Start()
    {
        LoadPreferences();
    }

    private void LoadPreferences()
    {
        PlayerPrefs preferences = GameManager.s_Instance.m_Preferences;

        m_ResolutionDropDown.ClearOptions();
        m_ResolutionDropDown.AddOptions(GameManager.s_Instance.m_ResolutionsString);

        int resolutionIndex = GameManager.s_Instance.CurrentResolutionIndex();
        m_ResolutionDropDown.value = resolutionIndex;
        SetResolution(resolutionIndex);

        m_FullscreenToggle.isOn = preferences.m_FullScreen;
        ToggleFullScreen(preferences.m_FullScreen);

        m_MusicSilder.value = preferences.m_MusicVolume;
        SetMusicVolume(preferences.m_MusicVolume);

        m_SfxSilder.value = preferences.m_SfxVolume;
        SetSfxVolume(preferences.m_SfxVolume);

        m_UISilder.value = preferences.m_UIVolume;
        SetSfxVolume(preferences.m_UIVolume);
    }

    public void SetResolution(int resolutionIndex) => GameManager.s_Instance.UpdateResolution(resolutionIndex);
    public void ToggleFullScreen(bool fullscreen) => GameManager.s_Instance.UpdateFullscreen(fullscreen);
    public void SetMusicVolume(float volume) => GameManager.s_Instance.UpdateMusicVolume(volume);
    public void SetSfxVolume(float volume) => GameManager.s_Instance.UpdateSfxVolume(volume);
    public void SetUIVolume(float volume) => GameManager.s_Instance.UpdateUIVolume(volume);
}
