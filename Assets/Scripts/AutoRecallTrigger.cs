﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRecallTrigger : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();
            if (!player.HoldingSword())
                FindObjectOfType<Sword>().m_CurrentState = Sword.States.recalled;
        }
    }
}
