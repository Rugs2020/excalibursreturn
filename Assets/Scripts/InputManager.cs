﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager s_Instance;

    [Header("Player Input")]
    public KeyCode m_Pause = KeyCode.Escape;
    public KeyCode m_Jump = KeyCode.Space;
    public KeyCode m_MoveLeft = KeyCode.A;
    public KeyCode m_MoveRight = KeyCode.D;
    public KeyCode m_Reset = KeyCode.R;

    public float m_MovementAxis = 0;

    private void Awake()
    {
        if (s_Instance != null)
        {
            Destroy(this);
            Debug.LogWarning("An instance of InputManager already exists in the scene. New instance has been destroyed.");
            return;
        }

        s_Instance = this;
    }

    private void Update()
    {
        if (PauseKeyDown())
            GameManager.s_Instance.TogglePause();

        UpdateMoveInput();
    }

    private void UpdateMoveInput()
    {
        float inputAxis = 0;
        if (Input.GetKey(m_MoveLeft))
            inputAxis -= 1;
        if (Input.GetKey(m_MoveRight))
            inputAxis += 1;

        m_MovementAxis = Mathf.Clamp(inputAxis, -1, 1);
    }

    public bool ResetKeyDown() { return Input.GetKey(m_Reset); }
    public bool JumpKeyDown() { return Input.GetKey(m_Jump); }
    public bool PauseKeyDown() { return Input.GetKeyDown(m_Pause); }
}
