﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class SceneTransition : MonoBehaviour
{
    [Tooltip("The name of the scene to be loaded\n NOTE: This is case sensitive and needs to be exact.")]
    public string m_SceneName = "";

    [Tooltip("If checked the scene will be loaded when the player contacts this GameObject's collider\n" +
             "NOTE: Leave this checked. Player input activation currently not implemented.")]
    public bool m_LoadOnCollision = true;

    private void Awake() => gameObject.GetComponent<BoxCollider>().isTrigger = true;
    public void LoadScene() => LevelLoader.LoadScene(m_SceneName);

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && other.GetComponent<Player>().HoldingSword())
        {
            if (m_LoadOnCollision)
                LoadScene();
            else
            {
                // Let player know they can interact with this object ??
            }
        }
    }
}
