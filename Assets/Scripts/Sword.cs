﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{

    [Space]
    [Header("Trajectory Variables:")]
    public float m_AirMoveSpeed = 5f;
    public float m_ThrowDistance = 10f;
    public float m_LookAheadTime = 0.45f;
    public int m_MaxVerticeAmount = 25;
    public float m_DistanceBetweenVertices = 0.5f;


    private float m_Gravity = -9.81f;
    private Vector3 m_InitVelocity = Vector3.zero;
    private Vector3 m_MultipiedInitVelocity;
    private Vector3 m_InitPosition;
    private List<Vector3> m_Path = new List<Vector3>();

    [Space]
    [Header("Recalling Variables:")]
    public float m_RecallMoveSpeed = 8f;
    public float m_RecallRotationSpeed = 24f;
    public float m_RecallGrabDistance = 1f;

    [Space]
    [Header("Embedding Variables:")]
    [Range(0f, 180)]
    public float m_EmbedAngleTolarence = 45f;
    public float m_EmbedAlignmentSpeed = 8f;
    public float m_EmbedDepth = 0.75f;

    [Space]
    [Header("References")]
    public LineRenderer m_LineRender;
    public CharacterController m_PlayerCharController;
    public Transform m_PlayerHandPos;
    public Camera m_Camera;

    private Vector3 m_EmbeddedNormal;
    private Vector3 m_EmbeddedPosition;

    private Vector3 m_MousePosition;

    private BoxCollider m_Collider;
    private Rigidbody m_Rigidbody;


    public enum States { held, readied, thrown, embedded, recalled, fallen };
    [HideInInspector]
    public States m_CurrentState = States.held;

    // needed to keep track of the time that has past when in midair
    private float m_ArcMovementTime;

    // Start is called before the first frame update
    void Start()
    {
        m_Collider = GetComponent<BoxCollider>();
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // get the velocity multiplied
        m_MultipiedInitVelocity = m_InitVelocity * m_ThrowDistance;

        // get mouse position
        m_MousePosition = m_Camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Mathf.Abs(m_Camera.transform.position.z - m_LineRender.transform.position.z)));
        m_MousePosition.z = 0f;

        // wow! state machine! :D
        switch (m_CurrentState)
        {
            // ============================================================================= Idling v
            case States.held:

                // set position to players hand
                transform.position = m_PlayerHandPos.position;
                transform.rotation = m_PlayerHandPos.rotation;

                break;

            // ============================================================================= Readied v
            case States.readied:

                // get the initial position
                m_InitPosition = m_LineRender.transform.position;

                // DEBUG =====================================================================
                m_InitVelocity.x = m_MousePosition.x - m_LineRender.transform.position.x;   //
                m_InitVelocity.y = m_MousePosition.y - m_LineRender.transform.position.y;   //
                m_InitVelocity.Normalize();                                                 //
                // ===========================================================================

                DrawArc(m_LineRender);

                // set position to players hand
                transform.position = m_PlayerHandPos.position;
                transform.rotation = m_PlayerHandPos.rotation;

                // reset the throw timer
                m_ArcMovementTime = 0.0f;
                break;

            // ============================================================================= Throwing v
            case States.thrown:
                // clear the arc
                m_LineRender.positionCount = 0;

                MoveAlongArc();

                RaycastHit hit;
                if (Physics.Raycast(transform.position, transform.up, out hit, 1f))
                {
                    if (hit.collider.CompareTag("Embeddable Surface"))
                    {
                        // get the angle that the raycast hit the collider
                        float hitAngle = Vector3.Angle(hit.normal, -transform.up);

                        if (hitAngle <= m_EmbedAngleTolarence)
                        {
                            // save the hit info for later use with embedding
                            m_EmbeddedNormal = hit.normal;
                            m_EmbeddedPosition = hit.point;

                            // change the state
                            m_CurrentState = States.embedded;
                        }
                        else
                        {
                            m_CurrentState = States.fallen;
                        }
                    }
                    else if (!hit.collider.CompareTag("Player"))
                    {
                        m_CurrentState = States.fallen;
                    }
                }

                break;

            // ============================================================================= Embedding v
            case States.embedded:

                // lerp the position and rotation to the new embedded 'transform'
                transform.position = Vector3.Lerp(transform.position, m_EmbeddedPosition + (m_EmbeddedNormal * m_EmbedDepth), m_EmbedAlignmentSpeed * Time.deltaTime);
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Quaternion.Euler(0f, 0f, -90f) * m_EmbeddedNormal, -m_EmbeddedNormal), m_EmbedAlignmentSpeed * Time.deltaTime);

                // enable the collision
                m_Rigidbody.isKinematic = true;

                // make sure not to enable it if the player is inside of the collsion box
                if (!m_Collider.enabled && !m_Collider.bounds.Intersects(m_PlayerCharController.bounds))
                {
                    m_Collider.enabled = true;
                }

                break;

            // ============================================================================= Recalling v
            case States.recalled:

                // move towards the player
                transform.position = Vector3.Lerp(transform.position, m_PlayerHandPos.position, m_RecallMoveSpeed * Time.deltaTime);

                // rotate the model towards the player
                Vector3 lookDir = (transform.position - m_PlayerHandPos.position).normalized;
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Quaternion.Euler(0f, 0f, 90f) * lookDir, lookDir), m_RecallRotationSpeed * Time.deltaTime);

                // if the swords close enough it will automatically set it's position to the players hand
                if (Vector3.Distance(transform.position, m_PlayerHandPos.position) <= m_RecallGrabDistance)
                {
                    // set position to players hand
                    transform.position = m_PlayerHandPos.position;
                    transform.rotation = m_PlayerHandPos.rotation;

                    m_CurrentState = States.held;
                }
                break;

            case States.fallen:

                // turn on them physics
                m_Collider.enabled = true;
                m_Rigidbody.isKinematic = false;

                break;
        }

        // don't collide with the player
        Physics.IgnoreCollision(m_Collider, m_PlayerCharController, m_CurrentState == States.fallen);

        // setting the parent to the players hand
        if (m_CurrentState == States.held || m_CurrentState == States.readied)
        {
            transform.parent = m_PlayerHandPos;
        }
        else
        {
            transform.parent = null;
        }

        // ==========================> DEBUG JUNK (Input) <==========================

        // left click stuff
        if (Input.GetMouseButtonDown(0) && m_CurrentState == States.held) // readying the sword
        {
            m_CurrentState = States.readied;
        }
        else if (Input.GetMouseButtonUp(0) && m_CurrentState == States.readied) // throwing the sword
        {
            m_CurrentState = States.thrown;
        }

        // right click stuff
        if (Input.GetMouseButtonDown(1))
        {
            // cancel throw
            if (m_CurrentState == States.readied)
                m_CurrentState = States.held;

            // recalling
            if (m_CurrentState == States.thrown || m_CurrentState == States.embedded || m_CurrentState == States.fallen)
                m_CurrentState = States.recalled;
        }

        // get rid of physics if not fallen
        if (m_CurrentState != States.fallen && m_CurrentState != States.embedded)
        {
            m_Collider.enabled = false;
            m_Rigidbody.isKinematic = true;
        }

        // ==================================================================
    }

    /// <summary>
    /// Returns the y point at a given time along a parabola that is generated by the m_InitVelocity and m_ThrowPower. 
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    private float GetParabolaYFromTime(float time)
    {
        float paraY = m_Gravity * (time * time) / 2 + m_MultipiedInitVelocity.y * time;
        return paraY;
    }

    /// <summary>
    /// Generates an arc on a LineRenderer using the m_InitVelocity and m_ThrowPower.
    /// </summary>
    public void DrawArc(LineRenderer lineRenderer)
    {
        // clear the path ready for the new one
        m_Path.Clear();

        // generate the curve for the linerenderer
        for (int i = 0; i < m_MaxVerticeAmount; ++i)
        {
            // calculate a new vertexs position relative to the player
            Vector3 newVertice = Vector3.zero;
            newVertice.x = m_DistanceBetweenVertices * i;
            newVertice.z = m_DistanceBetweenVertices * i;
            newVertice.y = GetParabolaYFromTime(newVertice.x);
            newVertice.x *= m_MultipiedInitVelocity.x;
            newVertice.z *= m_MultipiedInitVelocity.z;

            // check if it has hit a wall- if so end the trajectory
            if (m_Path.Count > 0)
            {
                RaycastHit hit;
                if (Physics.Raycast(lineRenderer.transform.position + m_Path[m_Path.Count - 1],
                    (newVertice - m_Path[m_Path.Count - 1]).normalized, out hit,
                    Vector3.Distance(m_Path[m_Path.Count - 1], newVertice)))
                {
                    // add the rays hit point to the path
                    m_Path.Add(hit.point - lineRenderer.transform.position);
                    break;
                }
            }

            // add the vertex to list
            m_Path.Add(newVertice);
        }

        // give linerenderer the path
        lineRenderer.positionCount = m_Path.Count;
        lineRenderer.SetPositions(m_Path.ToArray());
    }

    /// <summary>
    /// Moves the object this transforms on along an arc, using the m_InitVelocity and m_ThrowPower.
    /// </summary>
    public void MoveAlongArc()
    {
        m_ArcMovementTime += Time.deltaTime;

        // calculate a new position on a parabola
        Vector3 newPosition = Vector3.zero;
        newPosition.x = m_ArcMovementTime * m_AirMoveSpeed;
        newPosition.z = 0;
        newPosition.y = GetParabolaYFromTime(newPosition.x);
        newPosition.x *= m_MultipiedInitVelocity.x;

        // rotate toward a position along the parabola that is 0.65 seconds in the future
        Vector3 lookDir = Vector3.zero;
        lookDir.x = (m_ArcMovementTime + m_LookAheadTime) * m_AirMoveSpeed;
        lookDir.y = GetParabolaYFromTime(lookDir.x);
        lookDir.x *= m_MultipiedInitVelocity.x != 0f ? m_MultipiedInitVelocity.x : -0.15f;

        // the actual rotation
        transform.rotation = Quaternion.LookRotation(Quaternion.Euler(0f, 0f, 90f) * lookDir, lookDir);

        // apply new position
        transform.position = m_InitPosition + newPosition;
    }

}
