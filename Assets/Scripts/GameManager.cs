﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager s_Instance;
    public PlayerPrefs m_Preferences;
    public GameObject m_PauseMenu;

    public string m_WinScreenScene;

    public bool m_GamePaused = false;

    // Resolution stuff
    [HideInInspector]
    public List<string> m_ResolutionsString;
    public Resolution[] m_Resolutions;

    private void Awake()
    {
        if (s_Instance != null)
        {
            Destroy(this);
            Debug.LogWarning("An instance of GameManager already exists in the scene. New instance has been destroyed.");
            return;
        }

        s_Instance = this;

        InitResolutions();
    }

    private void Start()
    {
        LoadPreferences();
    }

    private void LoadPreferences()
    {
        UpdateResolution(CurrentResolutionIndex());
        UpdateFullscreen(m_Preferences.m_FullScreen);
        UpdateMusicVolume(m_Preferences.m_MusicVolume);
        UpdateSfxVolume(m_Preferences.m_SfxVolume);
        UpdateUIVolume(m_Preferences.m_UIVolume);
    }

    private void InitResolutions()
    {
        m_Resolutions = Screen.resolutions;
        m_ResolutionsString = new List<string>();

        for (int i = 0; i < m_Resolutions.Length; i++)
        {
            string option = $"{m_Resolutions[i].width} x {m_Resolutions[i].height}";

            if (m_ResolutionsString.Count == 0 || (m_ResolutionsString.Count > 0 && option != m_ResolutionsString[m_ResolutionsString.Count - 1]))
                m_ResolutionsString.Add(option);
        }
    }

    public int CurrentResolutionIndex()
    {
        for (int i = 0; i < m_Resolutions.Length; i++)
            if (m_Preferences.m_Resolution.Contains(m_Resolutions[i].width.ToString()) && m_Preferences.m_Resolution.Contains(m_Resolutions[i].height.ToString()))
                return i;

        return -1;
    }

    public void TogglePause()
    {
        if (InGame())
        {
            if (m_GamePaused)
            {
                if (m_PauseMenu.activeSelf)
                {
                    m_GamePaused = false;
                    m_PauseMenu.SetActive(false);
                    Time.timeScale = 1.0f;
                }
            }
            else
            {
                m_GamePaused = true;
                m_PauseMenu.SetActive(true);
                Time.timeScale = 0.0f;
            }
        }
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif

        Application.Quit();
    }

    public void UpdateResolution(int resolutionIndex)
    {
        Screen.SetResolution(m_Resolutions[resolutionIndex].width, m_Resolutions[resolutionIndex].height, Screen.fullScreen);
        m_Preferences.m_Resolution = $"{m_Resolutions[resolutionIndex].width} x {m_Resolutions[resolutionIndex].height}";
    }

    public void UpdateFullscreen(bool fullscreen)
    {
        Screen.fullScreen = fullscreen;
        m_Preferences.m_FullScreen = fullscreen;
    }

    public void UpdateMusicVolume(float volume)
    {
        m_Preferences.m_MusicVolume = volume;
        AudioManager.s_Instance.UpdateMusicVolume(volume);
    }

    public void UpdateSfxVolume(float volume)
    {
        m_Preferences.m_SfxVolume = volume;
        AudioManager.s_Instance.UpdateSfxVolume(volume);
    }

    public void UpdateUIVolume(float volume)
    {
        m_Preferences.m_UIVolume = volume;
        AudioManager.s_Instance.UpdateUIVolume(volume);
    }

    public void LoadEndScreen() => LevelLoader.LoadScene(m_WinScreenScene);
    public bool InGame() { return SceneManager.GetActiveScene().buildIndex != 0; }
    public void LoadMainMenu() => LevelLoader.LoadScene(0);
    public void LoadFirstScene() => LevelLoader.LoadScene(1);
    public void LoadScene(string levelName) => LevelLoader.LoadScene(levelName);
    public void ReloadScene() => LevelLoader.ReloadScene();
}
