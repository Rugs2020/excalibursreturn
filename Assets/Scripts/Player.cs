using System.Collections;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    #region References
    [Header("References + Tags")]
    [Tooltip("The character controller of the player\nNOTE: Automatically assigned if controller component is attatched to this gameobject")]
    public CharacterController m_Controller;

    public GameObject m_PlayerMesh;

    [Tooltip("The respawn/checkpoint object the player will spawn at if they die\nNOTE: Automatically assigned if a GameObject in the scene has the same tag as the 'Respawn Point Tag'")]
    public Transform m_RespawnPoint;

    [TagField]
    [Tooltip("The tag that defines which GameObjects will kill the player on contact")]
    public string m_KillObjectTag;

    [TagField]
    [Tooltip("The tag that defines the out of bounds object that will kill the player on contact")]
    public string m_WorldKillObjectTag;

    [Tooltip("The animator component attached to the character GameObject")]
    public Animator m_Animator;
    #endregion

    #region Player Properties
    [Header("Player Movement Properties")]
    [Min(1.0f)]
    [Tooltip("The acceleration amount of the player")]
    public float m_Acceleration = 10.0f;

    [Min(1.0f)]
    [Tooltip("The maximum speed the player can move")]
    public float m_MaxMoveSpeed = 10.0f;

    [Min(1.0f)]
    [Tooltip("The maximum speed the player can move while holkding the sword")]
    public float m_MaxMoveSpeedWithSword = 5.0f;

    [Min(0.0f)]
    [Tooltip("The amount of time that passes after player death before they respawn")]
    public float m_RespawnTime = 0.25f;

    [Header("Player Jumping Properties")]
    [Range(0.0f, 5.0f)]
    [Tooltip("Modifies the strength of gravity")]
    public float m_GravityModifier = 1.0f;
    [Min(0.0f)]
    [Tooltip("The maximum speed the player can fall")]
    public float m_MaxFallSpeed = 45.0f;

    [Min(0.0f)]
    [Tooltip("The jump height of the player")]
    public float m_BaseJumpHeight = 1.0f;

    [Min(0.0f)]
    [Tooltip("The amount of time it takes to reach the peak of the jump")]
    public float m_TimeToJumpPeak = 0.15f;

    [Min(0.0f)]
    [Tooltip("The maximum time the player can hold the jump to jump higher")]
    public float m_MaxJumpHoldTime = 0.175f;

    [Min(0.0f)]
    [Tooltip("The amount of time that passes before a player can no longer jump when falling off an object")]
    public float m_JumpForgivenessTime = 0.25f;

    private float m_FixedZPosition = 0;

    // G R A V I T Y (This is automatically assigned at Awake())
    private float m_Gravity = 0.0f;

    // The total force of the player's jump (This is automatically assigned at Awake())
    private float m_JumpForce = 0.0f;

    [HideInInspector]
    // This will be applied to the player's movement when they are attached to a platform *not in use right now*
    public Vector3 m_ExternalForce;

    // The player's current velocity NOTE: currently only tracks y axis...
    private Vector3 m_Velocity;
    #endregion

    #region Player Input Info
    [Header("Player Input Info (Updates at Runtime)")]
    [Tooltip("The movement direction of the player\n-1.0 - Left\n1 - Right")]
    public float m_HorizontalMoveAxis = 0.0f;

    [Tooltip("The amount of time passed after leaving platform")]
    public float m_ForgivenessTimer = 0.0f;

    [Tooltip("The amount of time player has held jump button")]
    public float m_JumpHoldTime = 0.0f;
    #endregion

    #region Checks
    [Header("Checks (Updates at Runtime)")]
    [Tooltip("Is the player alive (i.e. not in respawn or dying phase)")]
    public bool m_IsAlive = true;

    [Tooltip("Is the player standing on an object")]
    public bool m_IsGrounded = false;

    [Tooltip("Is the player currently jumping (travelling upwards during jump)")]
    public bool m_IsJumping = false;

    [Tooltip("Is the player currently falling")]
    public bool m_IsFalling = false;
    #endregion

    #region Debug
    [Header("Debug")]
    [Tooltip("When this is checked, debug gizmos will only be visible when this object is selected. Otherwise they are always visible.")]
    public bool m_DrawGizmosOnSelect = true;
    #endregion

    private void Awake()
    {
        // Assign the character controller if not already assigned
        if (m_Controller == null)
            m_Controller = gameObject?.GetComponent<CharacterController>();

        m_FixedZPosition = transform.position.z;

        // Calculate gravity and jump force
        m_Gravity = (-2.0f * m_BaseJumpHeight) / (m_TimeToJumpPeak * m_TimeToJumpPeak) * m_GravityModifier;
        m_JumpForce = 2.0f * m_BaseJumpHeight / m_TimeToJumpPeak;
    }

    private void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, m_FixedZPosition);

        // No need to update the game if paused
        if (GameManager.s_Instance.m_GamePaused)
            return;

        // This will continuously update the gravity and jump force of
        // the player to allow designers to tweak the variables at runtime
        CalculateGravity();
        CalculateJumpForce();

        // The ststus booleans in the inspector update here
        UpdateInspectorChecks();

        // Only do stuff if player is alive
        if (m_IsAlive)
            HandleInput();

        RotateMesh();
        Animate();
    }

    private void FixedUpdate()
    {
        // No need to update the game if paused
        if (GameManager.s_Instance.m_GamePaused)
            return;

        // Only do stuff if player is alive
        if (m_IsAlive)
        {
            CalculateMovement();
            Move();
        }
    }

    private void HandleInput()
    {
        // Cache player's input
        m_HorizontalMoveAxis = Input.GetAxisRaw("Horizontal");

        // Self explanatory
        // TO DO: Remove later or make editor tool if feeling spicy (probs not)
        if (InputManager.s_Instance.ResetKeyDown())
            GameManager.s_Instance.ReloadScene();

        if (IsGrounded())
        {
            ResetJump();

            // TO DO: Replace with input delegates
            if (InputManager.s_Instance.JumpKeyDown())
                Jump();
        }
        else
        {
            // TO DO: Replace with input delegates
            if (InputManager.s_Instance.JumpKeyDown())
                StopJump();

            if (IsFalling())
            {
                // Jump forgiveness timer
                m_ForgivenessTimer -= Time.deltaTime;
                if (m_ForgivenessTimer <= 0.0f)
                    m_ForgivenessTimer = 0.0f;
            }
        }
    }

    private void CalculateMovement()
    {
        // Change max move speed if the sword is being held
        float maxSpeed = HoldingSword() ? m_MaxMoveSpeedWithSword : m_MaxMoveSpeed;

        #region Calculate acceleration
        float acceleration = maxSpeed * m_Acceleration * Time.fixedDeltaTime;

        if (m_HorizontalMoveAxis != 0.0f)
        {
            // Calculate and clamp X velocity
            m_Velocity.x += acceleration * InputManager.s_Instance.m_MovementAxis;
            m_Velocity.x = Mathf.Clamp(m_Velocity.x, -maxSpeed, maxSpeed);
        }
        else
        {
            // Bring player to a halt over time
            if (m_Velocity.x < 0.0f)
            {
                m_Velocity.x += acceleration;

                if (m_Velocity.x > 0)
                    m_Velocity.x = 0;
            }
            else
            {
                m_Velocity.x -= acceleration;

                if (m_Velocity.x < 0)
                    m_Velocity.x = 0;
            }
        }
        #endregion

        if (IsGrounded() && !m_IsJumping)
        {
            m_Velocity.y = -Mathf.Abs(m_Gravity + m_MaxFallSpeed);

            PlatformCheck();
        }
        else if (!IsGrounded() && !IsFalling() && m_IsJumping)
            AddJumpHeight();
        else
            m_Velocity.y += m_Gravity * Time.fixedDeltaTime;

        // Clamp vertical velocity
        m_Velocity.y = Mathf.Clamp(m_Velocity.y, -m_MaxFallSpeed, m_JumpForce);

        // Prevents player from sliding off world collisions on z axis
        m_Velocity.z = 0;
    }

    private void RotateMesh()
    {
        // I hate that I had to use this again but this won't be necessary in gold.... hopefully
        if (m_PlayerMesh != null)
        {
            Vector3 direction;
            if (InputManager.s_Instance.m_MovementAxis != 0)
                direction = Vector3.right * InputManager.s_Instance.m_MovementAxis;
            else
                direction = Vector3.back;

            if (Mathf.Abs(m_Acceleration) > 0)
                m_PlayerMesh.transform.rotation = Quaternion.RotateTowards(m_PlayerMesh.transform.rotation, Quaternion.LookRotation(direction), 7.5f);
        }
    }

    private void Animate()
    {
        m_Animator.SetBool("HoldingSword", HoldingSword());
        m_Animator.SetBool("IsFalling", IsFalling());
        m_Animator.SetBool("IsJumping", m_IsJumping);
        m_Animator.SetBool("IsGrounded", IsGrounded());
        m_Animator.SetFloat("MoveAxis", Mathf.Abs(InputManager.s_Instance.m_MovementAxis));
    }

    private void PlatformCheck()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position - Vector3.up * m_Controller.radius, m_Controller.radius + 0.1f);

        foreach (Collider col in colliders)
        {
            EffectedObject platform = col.GetComponent<EffectedObject>();
            if (platform != null)
            {
                m_ExternalForce = platform.m_Movement;
                m_ExternalForce.z = 0;
                return;
            }
        }

        m_ExternalForce = Vector3.zero;
    }

    public void Move() => m_Controller.Move(m_ExternalForce + m_Velocity * Time.fixedDeltaTime);
    private void CalculateGravity() => m_Gravity = (-2.0f * m_BaseJumpHeight) / (m_TimeToJumpPeak * m_TimeToJumpPeak) * m_GravityModifier;
    private void CalculateJumpForce() => m_JumpForce = 2.0f * m_BaseJumpHeight / m_TimeToJumpPeak;
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Physics.IgnoreCollision(hit.collider, m_Controller, false);

        if (hit.collider.GetComponent<KillObject>() || (hit.transform.parent != null && hit.transform.parent.gameObject.GetComponent<KillObject>()))
            if (HoldingSword())
                Physics.IgnoreCollision(hit.collider, m_Controller, true);

        if (hit.collider.CompareTag(m_WorldKillObjectTag))
            KillPlayer();
    }

    #region Jumping Functions
    public void Jump()
    {
        if (!HoldingSword())
        {
            if (IsGrounded() && !m_IsJumping)
            {
                m_ForgivenessTimer = 0.0f;
                m_Velocity.y = m_JumpForce;
                m_IsJumping = true;
            }
            else if (!IsGrounded() && IsFalling() && !m_IsJumping)
            {
                if (m_ForgivenessTimer > 0.0f)
                {
                    m_ForgivenessTimer = 0.0f;
                    m_Velocity.y = m_JumpForce;
                    m_IsJumping = true;
                }
            }
        }
    }

    private void AddJumpHeight()
    {
        if (m_JumpHoldTime > 0.0f)
        {
            m_Velocity.y += m_JumpForce * Time.fixedDeltaTime;
            m_JumpHoldTime -= Time.fixedDeltaTime;
        }
        else
            StopJump();
    }

    public void ResetJump()
    {
        m_IsJumping = false;
        m_JumpHoldTime = m_MaxJumpHoldTime;
        m_ForgivenessTimer = m_JumpForgivenessTime;
    }

    public void StopJump()
    {
        m_IsJumping = false;
        m_JumpHoldTime = 0.0f;
        m_ForgivenessTimer = 0.0f;
    }
    #endregion

    #region Player Death + Respawn Functions
    public void KillPlayer()
    {
        // Change players alive state
        m_IsAlive = false;

        // Start the respawn timer
        StartCoroutine(RespawnTimer(m_RespawnTime));
    }

    public void RespawnPlayer()
    {
        // Reset player velocity
        m_Velocity = Vector3.zero;

        // Change players alive state
        m_IsAlive = true;
    }

    private IEnumerator RespawnTimer(float time)
    {
        // Wait for designated time before resetting player
        yield return new WaitForSeconds(time);

        GameManager.s_Instance.ReloadScene();
    }
    #endregion

    #region Check Functions
    private void UpdateInspectorChecks()
    {
        m_IsGrounded = IsGrounded();
        m_IsFalling = IsFalling();
    }
    public bool IsGrounded() { return m_Controller.isGrounded; }
    public bool IsFalling() { return m_Velocity.y < 0.0f; }
    public bool HoldingSword() { return transform.GetComponentInChildren<Sword>(); }
    public bool RespawnPointAssigned() => m_RespawnPoint != null;
    public bool KillObjectTagSet() => m_KillObjectTag != null && m_KillObjectTag != "";
    public bool WorldKillObjectTagSet() => m_WorldKillObjectTag != null && m_WorldKillObjectTag != "";
    #endregion

    #region Debug Functions
    private void OnDrawGizmos()
    {
        if (!m_DrawGizmosOnSelect)
        {
            if (RespawnPointAssigned())
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, m_RespawnPoint.position);
                Gizmos.DrawSphere(m_RespawnPoint.position, 0.25f);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (m_DrawGizmosOnSelect)
        {
            if (RespawnPointAssigned())
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, m_RespawnPoint.position);
                Gizmos.DrawSphere(m_RespawnPoint.position, 0.25f);
            }
        }
    }
    #endregion
}