﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(BoxCollider))]
public class InteractableObject : MonoBehaviour
{
    public enum InteractableType { pressurePlatePlayer, pressurePlatePlayerAndSword, keyStone, gear }
    public InteractableType m_InteractableType;

    public EffectedObject[] m_EffectedObjects;

    public float m_TimeUntilDeactivation = 1f;
    public bool m_PermanentlyActivated = false;

    // needed for gear
    private bool m_SwordEmbedded = false;
    private bool m_SwordRecalled = true;

    private bool m_Active = false;
    private float m_TimeToDeactivate = 0f;

    private void Awake()
    {
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    private void FixedUpdate()
    {
        if (m_Active)
            ActivateObjects();
        else
            DeactivateObjects();

        // deactivate if it can
        if (!m_PermanentlyActivated && m_TimeToDeactivate <= 0f)
            m_Active = false;

        // decrease the deactivate timer
        if (m_TimeToDeactivate > 0f)
            m_TimeToDeactivate -= Time.fixedDeltaTime;


        // the gear recall check
        // figure out if sword is recalled from this gameobject- this works in tandum with the OnTriggerStay gear if statement
        // [THIS IS ABSOLUTE GARBAGE BUT I THINK IT WORKS] - IT WORKS
        if (m_InteractableType == InteractableType.gear && !m_SwordEmbedded && !m_SwordRecalled)
        {
            m_Active = true;
            m_TimeToDeactivate = m_TimeUntilDeactivation;
            m_SwordRecalled = true;
        }
        else
            m_SwordEmbedded = false;
    }

    public void ActivateObjects()
    {
        // call the active function in each effected object
        foreach (EffectedObject effectedObject in m_EffectedObjects)
        {
            effectedObject.Activate();
        }
    }

    public void DeactivateObjects()
    {
        // call the deactive function in each effected object
        foreach (EffectedObject effectedObject in m_EffectedObjects)
        {
            effectedObject.Deactivate();
        }
    }

    private void OnTriggerStay(Collider other)
    {

        // pressure plate player and sword
        // Once the player and the sword are on the pressure plate, it activates.
        // Once one or both are off the pressure plate, it waits a amount of time and deactivates 
        if (m_InteractableType == InteractableType.pressurePlatePlayerAndSword && other.GetComponent<Player>() && other.GetComponent<Player>().HoldingSword() && other.GetComponent<Player>().IsGrounded())
        {
            m_Active = true;
            m_TimeToDeactivate = m_TimeUntilDeactivation;
        }
        // pressure plate player
        // Once the just the player is on the pressure plate, it activates.
        // its Permanently activated
        else if (m_InteractableType == InteractableType.pressurePlatePlayer && other.GetComponent<Player>() && other.GetComponent<Player>().IsGrounded())
        {
            m_Active = true;
            m_TimeToDeactivate = m_TimeUntilDeactivation;
        }
        // key stone
        // Once the sword is embedde, it activates.
        // Once it is recalled, it waits an amount of time and deactivates 
        else if (m_InteractableType == InteractableType.keyStone && other.GetComponent<Sword>() && other.GetComponent<Sword>().m_CurrentState == Sword.States.embedded)
        {
            m_Active = true;
            m_TimeToDeactivate = m_TimeUntilDeactivation;
        }
        // gear
        // After the sword is recalled from this object it will activate the effected objects
        else if (m_InteractableType == InteractableType.gear && other.GetComponent<Sword>() && other.GetComponent<Sword>().m_CurrentState == Sword.States.embedded)
        {
            m_SwordEmbedded = true;
            m_SwordRecalled = false;
        }
    }
}
