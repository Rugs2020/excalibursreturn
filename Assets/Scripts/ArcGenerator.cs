﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcGenerator : MonoBehaviour
{
    public LineRenderer m_LineRender;

    private Vector3 m_InitVelocity = new Vector3(0.5f, 0.5f, 0);
    private Vector3 m_MultipiedInitVelocity;
    private Vector3 m_InitPosition;

    private float m_Gravity = -9.81f;

    public float m_ThrowPower = 1f;
    public int m_VertexAmount = 25;
    public float m_DistanceBetweenVertexs = 0.5f;

    public List<Vector3> m_Path = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        // REMOVE THIS LATER
        m_InitPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //m_InitVelocity = transform.position - Camera.main.ScreenToViewportPoint(Input.mousePosition);
        m_InitVelocity.x = Input.GetAxis("Horizontal");
        m_InitVelocity.y = Input.GetAxis("Vertical");
        m_InitVelocity.Normalize();
        print(m_InitVelocity);

        m_Path.Clear();

        m_MultipiedInitVelocity = m_InitVelocity * m_ThrowPower;

        // generate the curve for the linerenderer
        for (int i = 0; i < m_VertexAmount; ++i)
        {
            // calculate a new vertexs position relative to the player
            Vector3 newVertex = Vector3.zero;
            newVertex.x = m_DistanceBetweenVertexs * i; // newVertex.x = m_InitPosition.x + (m_DistanceBetweenVertexs * i);
            newVertex.y = GetParabolaYFromTime(newVertex.x) - m_InitPosition.y;
            newVertex.x *= m_MultipiedInitVelocity.x;

            // add the vertex to list
            m_Path.Add(newVertex);
        }

        // give linerenderer the path
        m_LineRender.positionCount = m_Path.Count;
        m_LineRender.SetPositions(m_Path.ToArray());
    }

    private float GetParabolaYFromTime(float time)
    {
        float paraY = m_Gravity * (time*time) / 2 + m_MultipiedInitVelocity.y * time + m_InitPosition.y;
        return paraY;
    }
}
