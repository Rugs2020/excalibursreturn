﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class LevelLoader
{
    public static void LoadScene(string sceneName) => SceneManager.LoadSceneAsync(sceneName);
    public static void LoadScene(int sceneIndex) => SceneManager.LoadSceneAsync(sceneIndex);
    public static void LoadScene(Scene scene) => SceneManager.LoadSceneAsync(scene.name);
    public static void ReloadScene() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
}
