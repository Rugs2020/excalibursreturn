﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(TagFieldAttribute))]
public class TagField : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.propertyType == SerializedPropertyType.String)
        {
            // Draw a funky fresh tag field in the inspector because apparently it's not a unity default attribute ???
            EditorGUI.BeginProperty(position, label, property);
            property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
            EditorGUI.EndProperty();
        }
        else
        {
            /* Draws a regular property field with if a variable with
               the TagDropdown attribute above it is not a string
               NOTE: Without this it won't show up in the inspector */
            EditorGUI.PropertyField(position, property, label);
        }
    }
}